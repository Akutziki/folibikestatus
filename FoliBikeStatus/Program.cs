﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Exercise3
{
    /// <summary>
    /// Nouda Föli fillareiden pysäkkien data osoitteesta http://data.foli.fi/citybike
    /// Tee ohjelma joka kysyy käyttäjältä pysäkin tunnusta, näyttää kaikki valittavat pysäkit ja kuinka monta Föli fillaria on kyseisellä paikalla jäljellä
    /// Ohjelma tulostaa myös kuinka monta prosenttia valitun pysäkin pyöristä on käytössä
    /// Jos pysäkkiä ei löydy, ohjelma antaa käyttäjälle siitä tiedon
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            string rawjson = new WebClient().DownloadString("http://data.foli.fi/citybike"); //Fetch json from API
            JObject jObject = JObject.Parse(rawjson); //Parse json into JObject
            bool success = false;

            var attributes = jObject["racks"].ToList<JToken>(); //Add attributes to JToken list

            foreach (var stop in attributes)
            {
                JProperty jProperty = stop.ToObject<JProperty>();
                string propertyName = jProperty.Name;

                string rackName = jObject["racks"][propertyName]["name"].ToString(); //Fetch rack names from json
                string rackCode = jObject["racks"][propertyName]["stop_code"].ToString(); //Fetch rack codes's from json


                byte[] bytes = Encoding.Default.GetBytes(rackName); //Convert rack names to utf8 incase name has nordic characters
                rackName = Encoding.UTF8.GetString(bytes);

                Console.WriteLine(rackCode + ". " + rackName); //Print rack names and stop codes
            }

            Console.Write("\nMinkä Fölläriaseman tilanteen haluat tarkistaa?:  "); //Ask user which stations status he wants to check 

            string stopCode = Console.ReadLine();
            int allBikes = 0;
            int totalBikesBeingUsed = 0;

            foreach (JToken attribute in attributes) //Loop throught every Fölläri stop
            {
                JProperty jProperty = attribute.ToObject<JProperty>();
                string propertyName = jProperty.Name;

                allBikes = allBikes + Int32.Parse(jObject["racks"][propertyName]["slots_total"].ToString()); //Calculates amount of onique operators and how many of them are currently in use
                totalBikesBeingUsed = totalBikesBeingUsed + Int32.Parse(jObject["racks"][propertyName]["slots_avail"].ToString());

                if (string.Equals(jObject["racks"][propertyName]["stop_code"].ToString(), stopCode, StringComparison.OrdinalIgnoreCase)) //If stations stop_code matches parameters given by user
                {
                    Console.Write("Fölläreitä jäljellä: ");
                    Console.WriteLine(jObject["racks"][propertyName]["bikes_avail"]); //Print amount of available bikes
                    double bikesBeingUsed = Double.Parse(jObject["racks"][propertyName]["slots_avail"].ToString()); //slots_avail is same as how many bikes are currently away
                    double maxSlots = Double.Parse(jObject["racks"][propertyName]["slots_total"].ToString()); //slots_total determines how many bikes rack can hold
                    double bikesLeftPercentage = (bikesBeingUsed / maxSlots) * 100;
                    Console.WriteLine(Math.Round(bikesLeftPercentage, 2, MidpointRounding.AwayFromZero) + " Prosenttia käytössä valitun pysäkin pyöristä"); //Rounds and prints percentages
                    success = true; //Station was found
                }
            }
            if (!success) //If no station matched user given parameters, inform user
            {
                Console.WriteLine("Fölläriasemaa annetulla tunnuksella ei löytynyt");
            }
            Console.WriteLine(totalBikesBeingUsed + "/" + allBikes + " Pyörää käytössä kaikenkaikkiaan");
            Console.Read();
        }
    }
}









